#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on 2021/10/27
This code defines the erddap data structure and could be used to programatically 
produce dataset.xml snippets for datasets. 

The snippets can be concat'ed to create a erdadp compliant dataset.sml file.

The general <dataset> structure is:
<dataset type='X' datasetID='myID' active='true/false'>
    <some ERDDAP params>
    ...
    <addAttributes>
        <some dataset specific attributes>
        ...
    <dataVariable>
        <specifics on the datavariable 1>
    <dataVariable>
        <specifics on the datavariable 2>
    ...
</dataset>

@author: rory
"""

from marshmallow import Schema, fields, INCLUDE, ValidationError

class Attribute():
    def __init__(self, name, text):
        self.name = name
        self.text = text

class AttributeSchema(Schema):
    '''
    Single attribute: <att name="infoUrl">https://www.lifewatch.eu/</att>
    '''
    class Meta:
        include = {
            '@name': fields.Str(),
            '#text': fields.Str(),
                  }
        ordered = True


class att():
    def __init__(self, attribute_list):
        self.att = attribute_list
        
class attSchema(Schema):
    '''
    Collection of attributes:
    <addAttributes>
        <att name="cdm_data_type">Point</att>
        <att name="conventions"></att>
        ...
    </addAttributes>
    '''
    att = fields.Nested(AttributeSchema, many=True)


class dataVariable():
    def __init__(self, sourceName, destinationName, dataType, addAttributes = None):
        self.sourceName = sourceName
        self.destinationName = destinationName
        self.dataType = dataType
        self.addAttributes = addAttributes
        
class dataVariableSchema(Schema):
    '''
    Key/Value pairs for dataVariables;
    <dataVariable>
        <sourceName>BIOICE Sample number</sourceName>
        <destinationName>bioice_sample_number</destinationName>
        <dataType>int</dataType>
    </dataVariable>
    '''
    sourceName = fields.Str()
    destinationName = fields.Str()
    dataType = fields.Str()
    addAttributes = fields.Nested(attSchema)

class EDDTableFromAsciiFiles():
    def __init__(self, 
                 fileDir, 
                 attributes,
                 dataVariableList,
                 reloadEveryNMinutes = '10080', 
                 updateEveryNMillis= '0', 
                 fileNameRegex = '*.csv',
                 recursive = False, 
                 standardizeWhat = 0):
        self.fileDir = fileDir
        self.addAttributes = attributes
        self.dataVariable = dataVariableList
        self.reloadEveryNMinutes = reloadEveryNMinutes
        self.updateEveryNMillis = updateEveryNMillis
        self.fileNameRegex = fileNameRegex
        self.recursive = recursive
        self.standardizeWhat = standardizeWhat

class EDDTableFromAsciiFilesSchema(Schema):
    '''
    Super class for all EDDTablesFromXXX classes. The default values in each parameter are taken from the monstrous ERDDAPP 
    readme page: https://coastwatch.pfeg.noaa.gov/erddap/download/setupDatasetsXml.html 
    There seems to be a mix of storage parameters and default query parameters in the parent class. 
    
    TODO: test if representing ints as str is a problem for the XML translation
    TODO: Remove rarely used params. User should have to specify/add them directly. Strip this to the smallest usable class. 
    TODO: Refactor parent class params. There are many that only get used in certain child classes. 
    TODO: addAttributes seems like an important param. Need to spend some more time figuring it out.
    '''
    reloadEveryNMinutes = fields.Int() #Optional. defaults to 10080
    updateEveryNMillis = fields.Int() #Optional. defaults to 0
    fileDir =  fields.Str() # The directory with the data file/s. 
    fileNameRegex =  fields.Str() #Optional. Only files that comply to pathRegex will be accepted
    recursive = fields.Bool() #Optional. The fileDir has subdirectories with files
    standardizeWhat = fields.Int() #Optional. defaults to 0. This parameter is a mess. 
    
    addAttributes = fields.Nested(attSchema)
    dataVariable = fields.Nested(dataVariableSchema, many=True)     
    class Meta:
        include = {
            '@type': fields.Str(),
            '@datasetID': fields.Str(),
            '@active': fields.Str()}
        ordered = True

