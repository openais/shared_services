---
# Hugo K/V pairs 
title: Shared Services
comments: false
weight: 20
---

<!-- # Shared Services -->

These are some third-party services that are useful to have in the OpenAIS ecosystem. They are configured and deployed to the OpenAIS server in order to assist with management and operations.

Currently there are:
* PGAdmin: A tool for querying a postgresql databases
* GeoServer: A tool for sharing and publishing geospatial datasets. 
<!-- * Keycloak: an authentication service. Okay, this should probably be required but right now doesn't do anything.    -->

## Configure

An example configuration file is available at `./config/example.env`. This file must be copied to `.env` and edited to reflect your local environment. This is then used to deploy the docker-compose file via the command line, or preferably, Portainer.

The config variables are:
  - Image `TAG` : These are the image tags associated to the docker images used in the docker-compose file. 
  - Default User/Password credentials: These are the initial credentials that are used to access the services.
  - GeoServer Initial and Maximum memory: More information on Docker Geoserver config can be found [here](https://github.com/kartoza/docker-geoserver).

## Deployment

On Portainer navigate to Stacks>Add Stack:
  - Give it a name
  - Select "Repository" as build method
  - Input "https://gitlab.com/openais/shared_services" as the repository URL
  - Add Environment variables, either one by one or from a file that you've already edited to represent your deployment environment.
  - Deploy the stack


If using Docker Compose as the deployment method do the following:
```
git clone git@gitlab.com:openais/deployment/shared_services.git
cd ./shared_services
cp ./config/example.env .env
<edit the env file>
docker-compose up -d
```
